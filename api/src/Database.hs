{-# LANGUAGE OverloadedStrings #-}

module Database where

import Control.Monad.Logger (runStdoutLoggingT, LoggingT)
import Control.Monad.Reader (runReaderT)
import Data.Int (Int64)
import Data.Text (Text)
import Database.Persist (getEntity, insert, selectList, Entity)
import Database.Persist.Sqlite (fromSqlKey, toSqlKey, withSqliteConn, runMigration, SqlPersistT)

import Schema

-- In the future, we should get this from
-- either an environment variable or a config file.
localConnString :: Text
localConnString = "test.db"

runAction :: Text -> SqlPersistT (LoggingT IO) a -> IO a
runAction connectionString action =
  runStdoutLoggingT $ withSqliteConn connectionString $ \backend ->
    runReaderT action backend

migrateDB :: Text -> IO ()
migrateDB connString = runAction connString $ runMigration migrateAll

selectApples :: Text -> IO [Entity Apple]
selectApples connString = runAction connString $ selectList [] []

selectAppleById :: Text -> Int64 -> IO (Maybe (Entity Apple))
selectAppleById connString id = runAction connString $ getEntity (toSqlKey id)

createApple :: Text -> Apple -> IO Int64
createApple connString apple = fromSqlKey <$> (runAction connString $ insert apple)