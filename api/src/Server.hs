{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

module Server where

import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Except (throwE)
import Data.Int (Int64)
import Data.Text (Text)
import Database
import Database.Persist (Entity)
import Schema
import Servant

type Api =
      "apples" :> Get '[JSON] [Entity Apple]
 :<|> "apples" :> Capture "id" Int64 :> Get '[JSON] (Entity Apple)
 :<|> "apples" :> ReqBody '[JSON] Apple :> Post '[JSON] Int64

fetchApplesHandler :: Text -> Handler [Entity Apple]
fetchApplesHandler connString = do
      apples <- liftIO $ selectApples connString
      return apples

fetchAppleByIdHandler :: Text -> Int64 -> Handler (Entity Apple)
fetchAppleByIdHandler connString id = do
      maybeApple <- liftIO $ selectAppleById connString id
      case maybeApple of
            Just apple -> return apple
            Nothing -> Handler $ throwE $ err401 { errBody = "No apple with that ID" }

createAppleHandler :: Text -> Apple -> Handler Int64
createAppleHandler connString apple = liftIO $ createApple connString apple

server :: Text -> Server Api
server dbfile =
      fetchApplesHandler dbfile
 :<|> fetchAppleByIdHandler dbfile
 :<|> createAppleHandler dbfile

api :: Proxy Api
api = Proxy

app :: Text -> Application
app dbfile = serve api $ server dbfile
