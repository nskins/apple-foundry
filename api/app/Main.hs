{-# LANGUAGE OverloadedStrings #-}

module Main where

import Database
import Network.Wai.Handler.Warp
import Server

main :: IO ()
main = do
  let dbfile = "test.db"
  migrateDB dbfile
  run 3000 $ app dbfile
